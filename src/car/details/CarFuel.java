package car.details;

public enum CarFuel implements CarDetail {
    GASOLINE(0),
    DIESEL(10000),
    HYBRID(15000);

    private int fuelPrice;

    CarFuel(int fuelPrice){
        this.fuelPrice = fuelPrice;
    }

    @Override
    public int getPrice() {
        return fuelPrice;
    }
}
