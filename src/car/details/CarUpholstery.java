package car.details;

public enum CarUpholstery implements CarDetail {
    VELVET(0),
    LEATHER(1000),
    QUILTED_LEATHER(2000);

    private int upholsteryPrice;

    CarUpholstery(int upholsteryPrice){
        this.upholsteryPrice = upholsteryPrice;
    }

    @Override
    public int getPrice() {
        return upholsteryPrice;
    }
}
