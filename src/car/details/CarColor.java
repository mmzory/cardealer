package car.details;

public enum CarColor implements CarDetail {
    STANDARD_WHITE(0),
    STANDARD_BLACK(0),
    PREMIUM_GREEN(500),
    PREMIUM_PINK(2500),
    PREMIUM_GRAY(1500);

    private int colorPrice;

    CarColor(int colorPrice){
        this.colorPrice = colorPrice;
    }

    @Override
    public int getPrice() {
        return colorPrice;
    }
}
