package car.details;

public enum ChoosenCarDetail {
    BODY,
    COLOR,
    FUEL,
    UPHOLSTERY;
}
