package car.details;

public enum CarBody implements CarDetail {
    SEDAN(0),
    COMBI(1000),
    PICKUP(2000),
    HATCHBACK(1000);

    private int carPrice;

    CarBody(int carPrice){
        this.carPrice = carPrice;
    }

    @Override
    public int getPrice() {
        return carPrice;
    }
}
