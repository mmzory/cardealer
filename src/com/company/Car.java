package com.company;

import car.details.CarBody;
import car.details.CarColor;
import car.details.CarFuel;
import car.details.CarUpholstery;

public class Car {
    private String model;
    private int carPrice;
    private CarColor carColor;
    private CarUpholstery carUpholstery;
    private CarFuel carFuel;
    private CarBody carBody;

    public Car(){
        this.carBody = CarBody.SEDAN;
        this.carColor = CarColor.STANDARD_WHITE;
        this.carFuel = CarFuel.GASOLINE;
        this.carUpholstery = CarUpholstery.VELVET;
    }

    public Car(String model, int carPrice) {
        this.model = model;
        this.carPrice = carPrice;
        this.carColor = CarColor.STANDARD_BLACK;
        this.carUpholstery = CarUpholstery.VELVET;
        this.carFuel = CarFuel.GASOLINE;
        this.carBody = CarBody.SEDAN;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setCarPrice(int carPrice) {
        this.carPrice = carPrice;
    }

    public String getModel() {
        return model;
    }

    public int getCarPrice() {
        return carPrice;
    }

    public CarColor getCarColor() {
        return carColor;
    }

    public void setCarColor(CarColor carColor) {
        this.carColor = carColor;
    }

    public CarUpholstery getCarUpholstery() {
        return carUpholstery;
    }

    public void setCarUpholstery(CarUpholstery carUpholstery) {
        this.carUpholstery = carUpholstery;
    }

    public CarFuel getCarFuel() {
        return carFuel;
    }

    public void setCarFuel(CarFuel carFuel) {
        this.carFuel = carFuel;
    }

    public CarBody getCarBody() {
        return carBody;
    }

    public void setCarBody(CarBody carBody) {
        this.carBody = carBody;
    }
}
