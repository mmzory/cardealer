package com.company;

import car.details.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Salon {
    private List<Car> customerCars = new ArrayList<>();
    private SalonCars salonCars = new SalonCars();
    private Car car = new Car();
    private int lowestPrice = salonCars.getLowestPrice();
    private String notChoose = "brak wyboru";
    private String sWallet = new String();
    private short wrongChoice = 0;
    private int presentPrice = 0;
    private int wallet = 0;
    private boolean carChoosed = false;

    public void userMenu() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Witaj przybyszu, ile masz kasy na samochód?: ");
        sWallet = scanner.next();

        try {
            wallet = Integer.parseInt(sWallet);
            if (wallet < 0) {
                System.out.println("Wprowadzono minusowy budżet...");
                userMenu();
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Wprowadź kwotę!!");
            userMenu();
        }

        if (wallet < lowestPrice) {
            System.out.println("Sorki gościu nie stać Cię na żaden samochód...");
            System.exit(0);
        }

        System.out.println("Zacznij od wyboru modelu samochodu: ");
        car.setModel(chooseModel(wallet));

        boolean exit = false;

        do {
            setCarCost();
            System.out.println("Twój portfel ma wartość: " + wallet);
            System.out.println("Cena wybranego samochodu: " + presentPrice);
            System.out.println();
            System.out.println("Wybierz samochód:");
            System.out.println("1) Model (wybrany: " + (car.getModel() == null ? notChoose : car.getModel()) + ")" + "\n" +
                    "2) Typ nadwozia: " + (car.getModel() == null ? notChoose : car.getCarBody()) + "\n" +
                    "3) Kolor nadwozia: " + (car.getModel() == null ? notChoose : car.getCarColor()) + "\n" +
                    "4) Rodzaj paliwa: " + (car.getModel() == null ? notChoose : car.getCarFuel()) + "\n" +
                    "5) Rodzaj tapicerki: " + (car.getModel() == null ? notChoose : car.getCarUpholstery()) + "\n" +
                    "8) Pokaż kupione samochody" + "\n" +
                    "9) Zatwierdź wybór samochodu" + "\n" +
                    "0) Wyjście");

            String choice = scanner.next();

            switch (choice) {
                case "1":
                    car.setModel(chooseModel(wallet));
                    break;
                case "2":
                    setCarDetail(ChoosenCarDetail.BODY, CarBody.values());
                    break;
                case "3":
                    setCarDetail(ChoosenCarDetail.COLOR, CarColor.values());
                    break;
                case "4":
                    setCarDetail(ChoosenCarDetail.FUEL, CarFuel.values());
                    break;
                case "5":
                    setCarDetail(ChoosenCarDetail.UPHOLSTERY, CarUpholstery.values());
                    break;
                case "8":
                    showCustomerCars();
                    break;
                case "9":
                    addCarToCustomerList();
                    break;
                case "0":
                    exit = true;
                    break;
                default:
                    printWrongChoice();
            }
        } while (wallet >= lowestPrice && wrongChoice < 3 && !exit);
    }

    private String chooseModel(int wallet) {
        Scanner scanner = new Scanner(System.in);
        List<Car> availableCars = new ArrayList<>();
        for (Car car : salonCars.getCarList()) {
            if (car.getCarPrice() <= wallet) {
                availableCars.add(car);
            }
        }

        System.out.println("Samochody dostępne w Twoim budżecie: ");

        for (int i = 1; i <= availableCars.size(); i++) {
            System.out.println(i + ") " + availableCars.get(i - 1).getModel() + " - cena: " + availableCars.get(i - 1).getCarPrice());
        }

        if (carChoosed) {
            System.out.println("0) Powrót");
        }

        String tmpChoice = scanner.next();
        try {
            int choice = Integer.parseInt(tmpChoice);
            if (choice >= 1 && choice < availableCars.size() + 1) {
                carChoosed = true;
                car.setModel(availableCars.get(choice - 1).getModel());
                car.setCarPrice(availableCars.get(choice - 1).getCarPrice());
            } else if (choice == 0) {
                if (car.getModel() != null) {
                    setCarCost();
                } else {
                    return null;
                }
            } else {
                System.out.println("Wybór spoza zakresu!");
                System.out.println();
                chooseModel(wallet);
            }
        } catch (IllegalArgumentException e) {
            printWrongChoice();
            chooseModel(wallet);
        }

        if (car.getModel() != null && car.getModel().length() > 1) {
            return car.getModel();
        } else {
            return null;
        }
    }

    private void setCarDetail(ChoosenCarDetail choosenCarDetail, CarDetail[] carDetail) {
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < carDetail.length; i++) {
            System.out.println(i+1 + ") " + carDetail[i] + " (+" + carDetail[i].getPrice() + ")");
        }
        System.out.println("0) Powrót");
        String sChoice = scanner.next();
        int choice = 0;

        try {
            choice = Integer.parseInt(sChoice);
        } catch (IllegalArgumentException e) {
            System.out.println("Wprowadź liczbę!!");
            System.out.println();
            setCarDetail(choosenCarDetail, carDetail);
        }

        switch (choosenCarDetail){
            case BODY:
                if (choice > 0 && choice < CarBody.values().length+1) {
                    car.setCarBody(CarBody.values()[choice-1]);
                } else if (choice == 0) {
                } else {
                    printWrongChoice();
                    setCarDetail(choosenCarDetail, carDetail);
                }
                break;
            case COLOR:
                if (choice > 0 && choice < CarColor.values().length+1) {
                    car.setCarColor(CarColor.values()[choice-1]);
                } else if (choice == 0) {
                } else {
                    printWrongChoice();
                    setCarDetail(choosenCarDetail, carDetail);
                }
                break;
            case FUEL:
                if (choice > 0 && choice < CarFuel.values().length+1) {
                    car.setCarFuel(CarFuel.values()[choice-1]);
                } else if (choice == 0) {
                } else {
                    printWrongChoice();
                    setCarDetail(choosenCarDetail, carDetail);
                }
                break;
            case UPHOLSTERY:
                if (choice > 0 && choice < CarUpholstery.values().length+1) {
                    car.setCarUpholstery(CarUpholstery.values()[choice-1]);
                } else if (choice == 0) {
                } else {
                    printWrongChoice();
                    setCarDetail(choosenCarDetail, carDetail);
                }
                break;
            default:{
            }
        }
    }

    private void printWrongChoice(){
        System.out.println("Błędny wybór");
        System.out.println();
    }

    private void addCarToCustomerList() {
        if (car.getModel() != null) {
            if (presentPrice <= wallet) {
                customerCars.add(car);
                wallet -= presentPrice;
                car = new Car();
                setCarCost();
                if (wallet > lowestPrice) {
                    System.out.println("Zacznij od wyboru modelu samochodu: ");
                    carChoosed = false;
                    car.setModel(chooseModel(wallet));
                } else {
                    System.out.println("Nie stać Cię już na żaden samochód.");
                    showCustomerCars();
                }
            } else {
                if (wrongChoice < 2) {
                    System.out.println("Budżet za mały, zmień samochód lub wyposażenie i spróbuj jeszcze raz.");
                } else {
                    System.out.println("Po raz trzeci wybrałeś samochód, który jest dla Ciebie za drogi. \n" +
                            "Proszę opuścić salon...");
                }
                wrongChoice += 1;
            }
        } else {
            System.out.println("Nie wybrano samochodu!");
        }
        System.out.println();
        System.out.println();
    }

    private void setCarCost() {
        presentPrice = car.getCarPrice() +
                car.getCarBody().getPrice() +
                car.getCarColor().getPrice() +
                car.getCarFuel().getPrice() +
                car.getCarUpholstery().getPrice();
    }

    private void showCustomerCars() {
        if (customerCars.size() > 0) {
            System.out.println("Lista wybranych samochodów: ");
            for (int i = 1; i <= customerCars.size(); i++) {
                System.out.println(i + ") " + customerCars.get(i - 1).getModel() +
                        " - " + customerCars.get(i - 1).getCarBody() +
                        " - " + customerCars.get(i - 1).getCarColor() +
                        " - " + customerCars.get(i - 1).getCarFuel() +
                        " - " + customerCars.get(i - 1).getCarUpholstery());
            }
        } else {
            System.out.println("Nie kupiłeś jeszcze żadnego samochodu.");
        }
        System.out.println();
        System.out.println();
    }
}