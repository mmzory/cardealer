package com.company;

import java.util.ArrayList;
import java.util.List;

public class SalonCars {
    private List<Car> carList;

    public SalonCars(){
        carList = new ArrayList<>();
        addCar();
    }

    private void addCar(){
        carList.add(new Car("Astra", 32000));
        carList.add(new Car("Vectra", 60000));
        carList.add(new Car("Corsa", 48000));
        carList.add(new Car("Meriva", 26000));
        carList.add(new Car("Insignia", 120000));
    }

    public List<Car> getCarList(){
        return carList;
    }

    public int getLowestPrice(){
        int lowestPrice = carList.get(0).getCarPrice();
        for (Car car : carList){
            if (car.getCarPrice() < lowestPrice) {
                lowestPrice = car.getCarPrice();
            }
        }
        return lowestPrice;
    }
}
